package com.rpg.puzzle.domain.common.exception;

import static com.rpg.puzzle.rpg.domain.game.StaticMessages.VICTORY;

import com.rpg.puzzle.domain.character.Player;
import com.rpg.puzzle.domain.world.World;


public class Victory extends GameException {
    public Victory(String worldName, String playerName) {
        super(String.format(VICTORY, playerName, worldName));
    }

    public static void victory(World world, Player player) {
        throw new Victory(world.getName(), player.getName());
    }
}
