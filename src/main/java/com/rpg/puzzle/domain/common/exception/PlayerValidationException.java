package com.rpg.puzzle.domain.common.exception;

public class PlayerValidationException extends Exception {
    public PlayerValidationException(String message) {
        super(message);
    }
}
