package com.rpg.puzzle.domain.common.exception;

public class ShouldNeverHappen extends GameException {
    public ShouldNeverHappen() {
        super("Should never happen");
    }
}
