package com.rpg.puzzle.domain.common.exception;

import static com.rpg.puzzle.common.util.ColorFormatter.bold;
import static com.rpg.puzzle.common.util.ColorFormatter.red;

import com.rpg.puzzle.ports.exception.ConfigurationException;

public class LoadGameException extends ConfigurationException {
    public LoadGameException(Throwable cause) {
        super(bold(red("Failed to load game, please start a new one")), cause);
    }
}