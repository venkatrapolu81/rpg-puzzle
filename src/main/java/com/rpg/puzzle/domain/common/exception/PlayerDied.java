package com.rpg.puzzle.domain.common.exception;

import static com.rpg.puzzle.rpg.domain.game.StaticMessages.DEFEAT;

public class PlayerDied extends GameException {
    public PlayerDied() {
        super(DEFEAT);
    }
}
