package com.rpg.puzzle.domain.menu;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.rpg.puzzle.domain.common.exception.ShouldNeverHappen;
import com.rpg.puzzle.ports.exception.ConfigurationException;
import com.rpg.puzzle.ports.outgoing.GameStateProvider;
import com.rpg.puzzle.ports.outgoing.MainMenu;
import com.rpg.puzzle.ports.outgoing.RealmConfigurationProvider;
import com.rpg.puzzle.ports.outgoing.dto.MainMenuItem;
import com.rpg.puzzle.rpg.domain.game.GameManager;

import static com.rpg.puzzle.common.util.ColorFormatter.boldMagenta;
import static com.rpg.puzzle.ports.outgoing.dto.MainMenuItem.EXIT;

public class MainMenuManager {
    private static final Logger LOG = LogManager.getLogger(MainMenuManager.class);

    private final RealmConfigurationProvider configurationProvider;
    private final GameStateProvider gameStateProvider;
    private final AllMenus allMenus;
    private final MainMenu mainMenu;

    public MainMenuManager(RealmConfigurationProvider configurationProvider, AllMenus allMenus, GameStateProvider gameStateProvider) {
        this.allMenus = allMenus;
        this.mainMenu = allMenus.mainMenu();
        this.configurationProvider = configurationProvider;
        this.gameStateProvider = gameStateProvider;
    }

    public void showMenu() throws ConfigurationException {
        LOG.traceEntry();

        MainMenuItem choice;
        do {
            mainMenu.showMessage(boldMagenta("\nWelcome to Main Menu"));
            choice = mainMenu.showMenu();

            switch (choice) {
                case START:
                    GameManager.newGame(gameStateProvider, allMenus, configurationProvider.load());
                    break;
                case LOAD:
                    GameManager.loadGame(gameStateProvider, allMenus);
                    break;
                case EXIT:
                    mainMenu.showMessage("Come back soon! :)");
                    break;
                default:
                    throw new ShouldNeverHappen();
            }
        } while (EXIT != choice);


        LOG.traceExit();
    }


}
