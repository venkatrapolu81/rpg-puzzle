package com.rpg.puzzle.domain.world.location;

import static com.rpg.puzzle.common.util.ColorFormatter.boldMagenta;
import static com.rpg.puzzle.common.util.ColorFormatter.green;
import static com.rpg.puzzle.common.util.ColorFormatter.red;
import static com.rpg.puzzle.common.util.ColorFormatter.underlinedBlue;

import com.rpg.puzzle.common.util.ToStringBuilder;

public enum LocationType {

    EMPTY("Nothing is here", " "), 
    ALLY("A friend looks gently at you", green("A")),
    ENEMY("Enemy sighted!", red("E")),
    NPC_DEAD("Someone died here", boldMagenta("X")), 
    PLAYER("This is you, my dear Player", underlinedBlue("P"));


    private final String description;
    private final String mapMark;

    LocationType(String description, String mapMark) {
        this.description = description;
        this.mapMark = mapMark;
    }

    public String getDescription() {
        return description;
    }

    public String getMapMark() {
        return mapMark;
    }

    @Override
    public String toString() {
        return ToStringBuilder.defaultBuilder(this)
                .append("name", name())
                .append("mapMark", mapMark)
                .append("description", description)
                .build();
    }
}
