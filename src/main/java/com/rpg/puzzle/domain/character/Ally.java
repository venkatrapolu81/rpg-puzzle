package com.rpg.puzzle.domain.character;

import com.rpg.puzzle.common.util.Color;
import com.rpg.puzzle.domain.world.location.LocationType;

public class Ally extends NPC {
    public Ally(String name, String description, String greeting, int maxHp, int damage, int damageVariation) {
        super(name, description, greeting, maxHp, damage, damageVariation);
    }

    @Override
    protected LocationType locationTypeSpecificForNpc() {
        return LocationType.ALLY;
    }

    @Override
    protected Color greetingColor() {
        return Color.GREEN;
    }
}
