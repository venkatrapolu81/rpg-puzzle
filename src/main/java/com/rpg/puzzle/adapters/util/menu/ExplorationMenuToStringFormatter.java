package com.rpg.puzzle.adapters.util.menu;

import com.rpg.puzzle.ports.outgoing.dto.ExplorationMenuItem;

public class ExplorationMenuToStringFormatter extends MenuToStringFormatter<ExplorationMenuItem> {
    public ExplorationMenuToStringFormatter() {
    }

    public String format(ExplorationMenuItem item, int itemNumberInList) {
        return format(item.getDescription(), item.getKeyBinding());
    }
}
