package com.rpg.puzzle.adapters.util.menu;

import java.util.Arrays;

import com.rpg.puzzle.adapters.util.io.InputParser;
import com.rpg.puzzle.adapters.util.io.OutputWriter;

public class CliEnumMenuBase<ItemType extends Enum> extends CliMenuBase<ItemType> {
    protected CliEnumMenuBase(InputParser inputParser, OutputWriter outputWriter, ItemType[] menuItems) {
        super(inputParser, outputWriter, Arrays.asList(menuItems));
    }
}
