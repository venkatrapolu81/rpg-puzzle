package com.rpg.puzzle.adapters.outgoing;

import com.rpg.puzzle.adapters.util.io.InputParser;
import com.rpg.puzzle.adapters.util.io.OutputWriter;
import com.rpg.puzzle.adapters.util.menu.CliMenuBase;
import com.rpg.puzzle.domain.common.exception.PlayerValidationException;
import com.rpg.puzzle.ports.outgoing.PlayerConfigurationMenu;
import com.rpg.puzzle.ports.outgoing.dto.PlayerConfiguration;
import com.rpg.puzzle.ports.outgoing.dto.QuestionsToPlayer;

public class CliPlayerConfigurationMenu extends CliMenuBase<String> implements PlayerConfigurationMenu {

    public CliPlayerConfigurationMenu(InputParser inputParser, OutputWriter outputWriter) {
        super(inputParser, outputWriter);
    }

    @Override
    public PlayerConfiguration askForPlayerConfig(QuestionsToPlayer questions) {
        PlayerConfiguration playerConfiguration = null;

        PlayerConfiguration.PlayerConfigurationBuilder playerConfigurationBuilder = PlayerConfiguration.builder(questions.getMaxBonusPoints());
        playerConfigurationBuilder.withName(inputParser.readUserInputAsString(questions.getNameQuestion()));
        playerConfigurationBuilder.withDesc(inputParser.readUserInputAsString(questions.getDescQuestion()));

        do {
            try {
                showMessage(questions.getBonusStatsDescription());
                playerConfigurationBuilder.withHpBonus(inputParser.tryReadingInputAsInt(questions.getBonusStatsBonusHpQuestion()));
                playerConfigurationBuilder.withDamageBonus(inputParser.tryReadingInputAsInt(questions.getBonusStatsBonusDanageQuestion()));
                playerConfigurationBuilder.withDamageVariationBonus(inputParser.tryReadingInputAsInt(questions.getBonusStatsBonusDanageVariationQuestion()));

                playerConfiguration = playerConfigurationBuilder.build();
            } catch (PlayerValidationException e) {
                showMessage(e.getMessage());
            }
        } while (playerConfiguration == null);

        return playerConfiguration;
    }

    @Override
    public void showIntroduction(String introduction) {
        outputWriter.showMessageWithSpace(introduction);
    }

    @Override
    public void greetPlayer(String greetingMessage) {
        outputWriter.showMessageWithSpace(greetingMessage);
    }
}
