package com.rpg.puzzle.adapters.outgoing;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.rpg.puzzle.domain.common.exception.LoadGameException;
import com.rpg.puzzle.ports.exception.ConfigurationException;
import com.rpg.puzzle.ports.outgoing.GameStateProvider;
import com.rpg.puzzle.ports.outgoing.dto.GameState;

import static com.rpg.puzzle.rpg.domain.game.StaticMessages.GAME_LOADING_FAILED;

import java.util.List;

public class SerializedGameStateProvider extends SerializedResourceProvider<GameState> implements GameStateProvider {
    private static final Logger LOG = LogManager.getLogger(SerializedGameStateProvider.class);

    private static final String DEFAULT_SAVE_FILENAME = "save.ser";

    @Override
    public List<GameState> load() throws ConfigurationException {
        LOG.traceEntry();
        try {
            return super.load();
        } catch (ConfigurationException e) {
            throw new LoadGameException(e);
        }
    }

    @Override
    protected List<GameState> handleException(Exception e) throws ConfigurationException {
        throw new ConfigurationException(GAME_LOADING_FAILED, e);
    }

    @Override
    protected String getFilename() {
        return DEFAULT_SAVE_FILENAME;
    }

    @Override
    protected String basePath() {
        return savePath();
    }

    @Override
    protected boolean isLoadExternal() {
        return true;
    }
}
