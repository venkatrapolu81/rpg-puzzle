package com.rpg.puzzle.adapters.outgoing;

import java.util.List;

import com.rpg.puzzle.adapters.util.io.InputParser;
import com.rpg.puzzle.adapters.util.io.OutputWriter;
import com.rpg.puzzle.adapters.util.menu.CliMenuBase;
import com.rpg.puzzle.ports.outgoing.WorldConfigurationMenu;
import com.rpg.puzzle.ports.outgoing.dto.RealmConfiguration;

public class CliWorldConfigurationMenu extends CliMenuBase<RealmConfiguration> implements WorldConfigurationMenu {

    public CliWorldConfigurationMenu(InputParser inputParser, OutputWriter outputWriter) {
        super(inputParser, outputWriter);
    }

    @Override
    public RealmConfiguration chooseConfiguration(String realmQuestion, List<RealmConfiguration> realmConfigs) {
        setMenuItems(realmConfigs);

        printAllOptions(realmQuestion);
        return selectOption();
    }

    @Override
    public void confirmRealmConfiguration(String realmConfirmationMessage) {
        outputWriter.showMessageWithSpace(realmConfirmationMessage);
    }
}
