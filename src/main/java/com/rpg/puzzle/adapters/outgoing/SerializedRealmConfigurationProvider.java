package com.rpg.puzzle.adapters.outgoing;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.rpg.puzzle.common.generator.SerializationRealmConfigurationGenerator;
import com.rpg.puzzle.ports.outgoing.RealmConfigurationProvider;
import com.rpg.puzzle.ports.outgoing.dto.RealmConfiguration;

import static com.rpg.puzzle.adapters.util.io.ExternalIO.resourcesPath;

import java.util.List;

public class SerializedRealmConfigurationProvider extends SerializedResourceProvider<RealmConfiguration> implements RealmConfigurationProvider {
    private static final Logger LOG = LogManager.getLogger(SerializedRealmConfigurationProvider.class);

    public static final String FILENAME = "realm_configuration.ser";

    @Override
    protected String getFilename() {
        return FILENAME;
    }

    @Override
    protected String basePath() {
        return resourcesPath() + configPath();
    }

    @Override
    protected boolean isLoadExternal() {
        return false;
    }

    @Override
    protected List<RealmConfiguration> handleException(Exception e) {
        //throw new ConfigurationException("Could not load Realm Configuration", e);
        //TODO: decide what is best here.
        //with json an exception might have been a better option, but serialized files are not human readable anyway, so no one will update them
        return rollbackToBuiltInDefault();
    }

    private List<RealmConfiguration> rollbackToBuiltInDefault() {
        LOG.error("could not load realm configuration from a file, rolling back to defaults");
        return SerializationRealmConfigurationGenerator.realms();
    }
}
