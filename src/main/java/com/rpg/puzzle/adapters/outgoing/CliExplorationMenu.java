package com.rpg.puzzle.adapters.outgoing;

import com.rpg.puzzle.adapters.util.io.InputParser;
import com.rpg.puzzle.adapters.util.io.OutputWriter;
import com.rpg.puzzle.adapters.util.menu.CliEnumMenuBase;
import com.rpg.puzzle.adapters.util.menu.ExplorationMenuToStringFormatter;
import com.rpg.puzzle.adapters.util.menu.MenuToStringFormatter;
import com.rpg.puzzle.ports.outgoing.ExplorationMenu;
import com.rpg.puzzle.ports.outgoing.dto.ExplorationMenuItem;

public class CliExplorationMenu extends CliEnumMenuBase<ExplorationMenuItem> implements ExplorationMenu {
    public CliExplorationMenu(InputParser inputParser, OutputWriter outputWriter) {
        super(inputParser, outputWriter, ExplorationMenuItem.values());
    }

    @Override
    public void showMap(String map) {
        outputWriter.showMessageWithSpace(map);
    }

    @Override
    public void showStatistics(String statistics) {
        outputWriter.showMessageWithSpace(statistics);
    }

    @Override
    protected MenuToStringFormatter<ExplorationMenuItem> menuFormatter() {
        return new ExplorationMenuToStringFormatter();
    }

    @Override
    protected ExplorationMenuItem readUserChoice() {
        ExplorationMenuItem choice = ExplorationMenuItem.fromString(inputParser.readUserInputAsString());
        if (null == choice) {
            showMessage("You have chosen a wrong option, please try again");
            return ExplorationMenuItem.COMMANDS;
        }

        return choice;
    }

}
