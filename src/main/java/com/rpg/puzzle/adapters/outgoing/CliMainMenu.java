package com.rpg.puzzle.adapters.outgoing;

import com.rpg.puzzle.adapters.util.io.InputParser;
import com.rpg.puzzle.adapters.util.io.OutputWriter;
import com.rpg.puzzle.adapters.util.menu.CliEnumMenuBase;
import com.rpg.puzzle.ports.outgoing.MainMenu;
import com.rpg.puzzle.ports.outgoing.dto.MainMenuItem;

public class CliMainMenu extends CliEnumMenuBase<MainMenuItem> implements MainMenu {

    public CliMainMenu(InputParser inputParser, OutputWriter outputWriter) {
        super(inputParser, outputWriter, MainMenuItem.values());
    }

    @Override
    public MainMenuItem showMenu() {
        printAllOptions();
        return selectOption();
    }
}
