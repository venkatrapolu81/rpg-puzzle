package com.rpg.puzzle.adapters.outgoing;

import com.rpg.puzzle.adapters.util.io.InputParser;
import com.rpg.puzzle.adapters.util.io.OutputWriter;
import com.rpg.puzzle.adapters.util.menu.CliEnumMenuBase;
import com.rpg.puzzle.ports.outgoing.FightMenu;
import com.rpg.puzzle.ports.outgoing.dto.FightMenuItem;

public class CliFightMenu extends CliEnumMenuBase<FightMenuItem> implements FightMenu {
    public CliFightMenu(InputParser inputParser, OutputWriter outputWriter) {
        super(inputParser, outputWriter, FightMenuItem.values());
    }
}
