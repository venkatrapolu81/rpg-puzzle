package com.rpg.puzzle.adapters.outgoing;

import com.rpg.puzzle.adapters.util.io.InputParser;
import com.rpg.puzzle.adapters.util.io.OutputWriter;
import com.rpg.puzzle.adapters.util.menu.CliEnumMenuBase;
import com.rpg.puzzle.ports.outgoing.BeforeFightMenu;
import com.rpg.puzzle.ports.outgoing.dto.BeforeFightMenuItem;

public class CliBeforeFightMenu extends CliEnumMenuBase<BeforeFightMenuItem> implements BeforeFightMenu {
    public CliBeforeFightMenu(InputParser inputParser, OutputWriter outputWriter) {
        super(inputParser, outputWriter, BeforeFightMenuItem.values());
    }
}
