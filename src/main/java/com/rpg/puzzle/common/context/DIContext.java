package com.rpg.puzzle.common.context;

import java.util.HashMap;
import java.util.Map;

import com.rpg.puzzle.adapters.outgoing.CliBeforeFightMenu;
import com.rpg.puzzle.adapters.outgoing.CliExplorationMenu;
import com.rpg.puzzle.adapters.outgoing.CliFightMenu;
import com.rpg.puzzle.adapters.outgoing.CliMainMenu;
import com.rpg.puzzle.adapters.outgoing.CliPlayerConfigurationMenu;
import com.rpg.puzzle.adapters.outgoing.CliWorldConfigurationMenu;
import com.rpg.puzzle.adapters.outgoing.SerializedGameStateProvider;
import com.rpg.puzzle.adapters.outgoing.SerializedRealmConfigurationProvider;
import com.rpg.puzzle.adapters.util.io.InputParser;
import com.rpg.puzzle.adapters.util.io.OutputWriter;
import com.rpg.puzzle.domain.common.exception.DIException;
import com.rpg.puzzle.domain.menu.AllMenus;
import com.rpg.puzzle.domain.menu.MainMenuManager;
import com.rpg.puzzle.ports.outgoing.BeforeFightMenu;
import com.rpg.puzzle.ports.outgoing.ExplorationMenu;
import com.rpg.puzzle.ports.outgoing.FightMenu;
import com.rpg.puzzle.ports.outgoing.GameStateProvider;
import com.rpg.puzzle.ports.outgoing.MainMenu;
import com.rpg.puzzle.ports.outgoing.PlayerConfigurationMenu;
import com.rpg.puzzle.ports.outgoing.RealmConfigurationProvider;
import com.rpg.puzzle.ports.outgoing.WorldConfigurationMenu;

public class DIContext {
    private static final Map<Class, Object> context = new HashMap<>();

    static {
        consoleIo();
        configurationProviders();
        loadSaveGame();
        menus();
        managers();
    }

    private DIContext() {
    }

    private static void consoleIo() {
        OutputWriter outputWriter = new OutputWriter(System.out);
        extendContext(OutputWriter.class, outputWriter);

        InputParser inputParser = new InputParser(outputWriter, System.in);
        extendContext(InputParser.class, inputParser);
    }

    private static void configurationProviders() {
        RealmConfigurationProvider realmConfigurationProvider = new SerializedRealmConfigurationProvider();
        extendContext(RealmConfigurationProvider.class, realmConfigurationProvider);
    }

    private static void loadSaveGame() {
        GameStateProvider gameStateProvider = new SerializedGameStateProvider();
        extendContext(GameStateProvider.class, gameStateProvider);
    }

    private static void menus() {
        OutputWriter outputWriter = getBean(OutputWriter.class);
        InputParser inputParser = getBean(InputParser.class);


        PlayerConfigurationMenu playerConfigurationMenu = new CliPlayerConfigurationMenu(inputParser, outputWriter);
        extendContext(PlayerConfigurationMenu.class, playerConfigurationMenu);

        WorldConfigurationMenu worldConfigurationMenu = new CliWorldConfigurationMenu(inputParser, outputWriter);
        extendContext(WorldConfigurationMenu.class, worldConfigurationMenu);

        MainMenu mainMenu = new CliMainMenu(inputParser, outputWriter);
        extendContext(MainMenu.class, mainMenu);

        ExplorationMenu explorationMenu = new CliExplorationMenu(inputParser, outputWriter);
        extendContext(ExplorationMenu.class, explorationMenu);

        FightMenu fightMenu = new CliFightMenu(inputParser, outputWriter);
        extendContext(FightMenu.class, fightMenu);

        BeforeFightMenu beforeFightMenu = new CliBeforeFightMenu(inputParser, outputWriter);
        extendContext(BeforeFightMenu.class, beforeFightMenu);

        AllMenus allMenus = new AllMenus(mainMenu, playerConfigurationMenu, worldConfigurationMenu, explorationMenu, beforeFightMenu, fightMenu);
        extendContext(AllMenus.class, allMenus);
    }

    private static void managers() {
        AllMenus allMenus = getBean(AllMenus.class);
        GameStateProvider gameStateProvider = getBean(GameStateProvider.class);
        RealmConfigurationProvider realmConfigurationProvider = getBean(RealmConfigurationProvider.class);

        MainMenuManager mainMenuManager = new MainMenuManager(realmConfigurationProvider, allMenus, gameStateProvider);
        extendContext(MainMenuManager.class, mainMenuManager);
    }

    private static void extendContext(Class type, Object impl) {
        context.put(impl.getClass(), impl);
        context.put(type, impl);
    }

    @SuppressWarnings("unchecked")
    public static <T> T getBean(Class<T> clazz) {
        T bean;

        try {
            bean = (T) context.get(clazz);
        } catch (Throwable t) {
            throw new DIException(t);
        }

        if (null == bean) {
            throw new DIException();
        }

        return bean;
    }
}
