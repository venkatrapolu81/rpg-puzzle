package com.rpg.puzzle.ports.outgoing;

import com.rpg.puzzle.ports.outgoing.dto.PlayerConfiguration;
import com.rpg.puzzle.ports.outgoing.dto.QuestionsToPlayer;

public interface PlayerConfigurationMenu {
  
    PlayerConfiguration askForPlayerConfig(QuestionsToPlayer questionsToPlayer);

    void showIntroduction(String introduction);

    void greetPlayer(String greetingMessage);
}
