package com.rpg.puzzle.ports.outgoing;

import java.util.Collections;
import java.util.List;

import com.rpg.puzzle.ports.exception.ConfigurationException;
import com.rpg.puzzle.ports.outgoing.dto.GameState;


public interface ResourceProvider<ItemType> {
    List<ItemType> load() throws ConfigurationException;

    void save(List<ItemType> resource) throws ConfigurationException;

    default ItemType loadOne() throws ConfigurationException {
        List<ItemType> load = load();
        if (load.size() > 1) {
            throw new ConfigurationException("Only one entry permitted for this resource");
        }

        return load.get(0);
    }

    default void saveOne(ItemType resource) throws ConfigurationException {
        List<ItemType> listWithOneResource = Collections.singletonList(resource);
        save(listWithOneResource);
    }
}
