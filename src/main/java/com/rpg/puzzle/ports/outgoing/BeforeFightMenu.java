package com.rpg.puzzle.ports.outgoing;

import com.rpg.puzzle.ports.outgoing.dto.BeforeFightMenuItem;

public interface BeforeFightMenu extends BaseMenu<BeforeFightMenuItem> {
}
