package com.rpg.puzzle.ports.outgoing.dto;

import java.io.Serializable;

import com.rpg.puzzle.domain.character.Player;
import com.rpg.puzzle.domain.world.World;

public class GameState implements Serializable {
    private final World world;
    private final Player player;

    public GameState(World world, Player player) {
        this.world = world;
        this.player = player;
    }

    public World getWorld() {
        return world;
    }

    public Player getPlayer() {
        return player;
    }
}
