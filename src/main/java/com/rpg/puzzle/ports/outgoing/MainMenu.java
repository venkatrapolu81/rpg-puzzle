package com.rpg.puzzle.ports.outgoing;

import com.rpg.puzzle.ports.outgoing.dto.MainMenuItem;

public interface MainMenu extends BaseMenu<MainMenuItem> {
}
