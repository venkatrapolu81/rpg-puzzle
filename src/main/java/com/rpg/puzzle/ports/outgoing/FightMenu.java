package com.rpg.puzzle.ports.outgoing;

import com.rpg.puzzle.ports.outgoing.dto.FightMenuItem;

public interface FightMenu extends BaseMenu<FightMenuItem> {
}
