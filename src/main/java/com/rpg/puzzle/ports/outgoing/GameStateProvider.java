package com.rpg.puzzle.ports.outgoing;

import com.rpg.puzzle.ports.exception.ConfigurationException;
import com.rpg.puzzle.ports.outgoing.dto.GameState;


public interface GameStateProvider extends ResourceProvider<GameState> {
    default GameState loadGame() throws ConfigurationException {
        return loadOne();
    }

    default void saveGame(GameState gameState) throws ConfigurationException {
        saveOne(gameState);
    }
}
