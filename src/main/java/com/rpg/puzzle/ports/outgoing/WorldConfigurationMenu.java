package com.rpg.puzzle.ports.outgoing;

import java.util.List;

import com.rpg.puzzle.ports.outgoing.dto.RealmConfiguration;

public interface WorldConfigurationMenu {
    RealmConfiguration chooseConfiguration(String realmQuestion, List<RealmConfiguration> realmConfigs);

    void confirmRealmConfiguration(String realmConfirmationMessage);
}
