package com.rpg.puzzle.ports.outgoing;

import com.rpg.puzzle.ports.outgoing.dto.ExplorationMenuItem;

public interface ExplorationMenu extends BaseMenu<ExplorationMenuItem> {

    
    void showMap(String map);

    
    void showStatistics(String statistics);
}
