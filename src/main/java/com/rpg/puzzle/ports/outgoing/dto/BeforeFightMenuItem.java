package com.rpg.puzzle.ports.outgoing.dto;

import static com.rpg.puzzle.common.util.ColorFormatter.blue;
import static com.rpg.puzzle.common.util.ColorFormatter.red;

public enum BeforeFightMenuItem {
    ATTACK(red("Attack!!!")),
    FALL_BACK(blue("Fall back from this fight."));
    private final String description;

    BeforeFightMenuItem(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return description;
    }
}
