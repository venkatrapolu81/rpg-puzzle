package com.rpg.puzzle.ports.outgoing;

import com.rpg.puzzle.ports.outgoing.dto.RealmConfiguration;

public interface RealmConfigurationProvider extends ResourceProvider<RealmConfiguration> {

}
