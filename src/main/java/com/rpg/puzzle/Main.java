package com.rpg.puzzle;

import com.rpg.puzzle.adapters.incoming.CliGameLauncher;

public class Main {

    public static void main(String[] args) {
        CliGameLauncher.startGame();
    }
}
